import { Component, OnInit } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable,of } from 'rxjs';
import { map, startWith,catchError } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { SearchService } from './app-search.service';

const STATE_KEY_ITEMS = makeStateKey('items');
const STATE_SEARCH = makeStateKey('search');

@Component({
  selector: 'app-app-search',
  templateUrl: './app-search.component.html',
  styleUrls: ['./app-search.component.css']
})
export class AppSearchComponent implements OnInit {

  separatorKeysCodes: number[] = [ENTER, COMMA];
  languageCtrl = new FormControl();
  filteredLanguages: Observable<string[]>;
  allLanguages: string[] = ['JavaScript', 'TypeScript', 'ClojureScript', 'ReasonML', 'Python', 'Php', 'HTML'];
  addOnBlur = true;
  searchState:any = {};
  topics: string[];
  languages: string[];
  advanced: Boolean;
  username: String;
  organisation: String;
  searchPhrase: String;
  searchInName: Boolean;
  searchInDescription: Boolean;
  searchInReadme: Boolean;
  items: any = [];
  loaded: boolean;
  loading: boolean;
  errors: any = [];
  starsMin: number;
  starsMax: number;
  starsSelect: string = '';
  sizeMin: number;
  sizeMax: number;
  sizeSelect: string;
  dateMin: Date;
  dateMax: Date;
  dateSelect: string;
  pageEvent: PageEvent;
  pages: number;
  itemsPerPage: number = 10;
  sortSelect: string = 'default';
  orderSelect: string = 'desc';
  page: number = 1;
  @ViewChild('languageInput') languageInput: ElementRef<HTMLInputElement>;

  constructor(private state: TransferState,
    private itemsService: SearchService) {
    this.items = this.state.get(STATE_KEY_ITEMS, <any>[]);
    this.searchState = this.state.get(STATE_SEARCH, <any>{});
    this.advanced =this.searchState.advanced || false;
    this.searchPhrase =this.searchState.searchPhrase || "";
    this.username = this.searchState.username || '';
    this.organisation =this.searchState.organisation || "";
    this.topics = this.searchState.topics || [];
    this.searchInName=this.searchState.searchInName;
    this.searchInDescription=this.searchState.searchInDescription;
    this.searchInReadme=this.searchState.searchInReadme;
    this.sortSelect =this.searchState.sortSelect || 'default';
    this.orderSelect =this.searchState.orderSelect || 'desc';
    this.page =this.searchState.page || 1;
    this.pages =this.searchState.pages || 1;
    this.searchInName =this.searchState.searchInName || true;
    this.searchInDescription =this.searchState.searchInDescription || false;
    this.searchInReadme =this.searchState.searchInReadme || false;
    this.starsMin =this.searchState.starsMin || null;
    this.starsMax =this.searchState.starsMax || null;
    this.sizeMin =this.searchState.sizeMin || null;
    this.sizeMax =this.searchState.sizeMax || null;
    this.starsSelect =this.searchState.starsSelect || '';
    this.sizeSelect =this.searchState.sizeSelect || '';
    this.dateSelect =this.searchState.dateSelect || '';
    this.dateMin =this.searchState.dateMin || null;
    this.dateMax =this.searchState.dateMax || null;
    this.loaded = false;
    this.loading = false;

    this.filteredLanguages = this.languageCtrl.valueChanges.pipe(
      startWith(null),
      map((language: string | null) => (language ? this._filterLanguage(language) : this.allLanguages.slice())),
    );
    
   this.languages = this.searchState.languages || []; 

  }

  ngOnInit(): void {
    if(this.items.length>0){this.loaded = true}
 }

  saveState(prop:string,value:any): void{
    this.searchState[prop] =value;
    this.state.set(STATE_SEARCH, <any>this.searchState);
  }

  pagination(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    this.search();
  }

  search(): void {
    let q: string = "";
    this.errors = [];
    if (this.searchPhrase && this.searchPhrase.length > 2) {
      if(!this.searchInName && !this.searchInDescription && !this.searchInReadme){
        this.errors.push("At least one checkbox is required to check");
        return;
      }
      q += this.searchPhrase + " in:";
      if (this.searchInName) {
        q += "name";
      }
      if (this.searchInDescription) {
        if (this.searchInName) { q += "," }
        q += "description";
      }
      if (this.searchInReadme) {
        if (this.searchInDescription || this.searchInName) { q += "," }
        q += "readme"
      }
    }
    if (this.username && this.username.length > 2) {
      if (q.length > 0) { q += "+" }
      q += "user:" + this.username;
    }
    if (this.organisation && this.organisation.length > 2) {
      if (q.length > 0) { q += "+" }
      q += "org:" + this.organisation;
    }
    if (this.languages && this.languages.length > 0) {
      if (q.length > 0) { q += "+" }
      q += "language:" + this.languages[0];
      if (this.languages.length > 1) {
        for (let index = 1; index < this.languages.length; index++) {
          q += "," + this.languages[index];
        }
      }
    }
    if (this.topics && this.topics.length > 0) {
      if (q.length > 0) { q += "+" }
      q += "topic:" + this.topics[0];
      if (this.topics.length > 1) {
        for (let index = 1; index < this.topics.length; index++) {
          q += "," + this.topics[index];
        }
      }
    }

    if (this.starsSelect != "" && this.starsMin) {
      if (q.length > 0) { q += "+" }
      q += "stars:";
      switch (this.starsSelect) {
        case 'equal': {
          q += this.starsMin;
          break;
        }
        case 'greater': {
          q += '>' + this.starsMin;
          break;
        }
        case 'less': {
          q += '<' + this.starsMin;
          break;
        }
        case 'between': {
          q += this.starsMin + ".." + this.starsMax;
          break;
        }
      }
    }

    if (this.sizeSelect != "" && this.sizeMin) {
      if (q.length > 0) { q += "+" }
      q += "size:";
      switch (this.sizeSelect) {
        case 'equal': {
          q += this.sizeMin;
          break;
        }
        case 'greater': {
          q += '>' + this.sizeMin;
          break;
        }
        case 'less': {
          q += '<' + this.sizeMin;
          break;
        }
        case 'between': {
          q += this.sizeMin + ".." + this.sizeMax;
          break;
        }
      }
    }
    if (this.dateSelect != "" && this.dateMin) {
      if (q.length > 0) { q += "+" }
      q += "created:";
      switch (this.dateSelect) {
        case 'before': {
          q += '<' + this.dateMin.toISOString();
          break;
        }
        case 'onorbefore': {
          q += '<=' + this.dateMin.toISOString();
          break;
        }
        case 'after': {
          q += '>' + this.dateMin.toISOString();
          break;
        }
        case 'onorafter': {
          q += '>=' + this.dateMin.toISOString();
          break;
        }
        case 'between': {
          q += this.dateMin.toISOString() + ".." + this.dateMax.toISOString();
          break;
        }
      }
    }
    if (q.length > 0) {
      q += '&sort=' + this.sortSelect + '&order=' + this.orderSelect + '&per_page=' + this.itemsPerPage + '&page=' + this.page;
      this.searchState.sortSelect = this.sortSelect;
      this.searchState.orderSelect = this.orderSelect;
      this.searchState.page = this.page;
    }

    if (q.length > 0) {
      this.loaded = false;
      this.loading = true;
      this.itemsService.getItems('https://api.github.com/search/repositories?q=' + q)
        .pipe(
          catchError(val => of(`I caught: ${val}`))
        )
        .subscribe(
          (res: any) => { 
            this.items = res.items || [];
            this.loaded = true;
            this.loading = false;
            this.pages = res.total_count;
            this.searchState.pages = this.pages;
            this.state.set(STATE_KEY_ITEMS, <any>res.items);
            this.state.set(STATE_SEARCH, <any>this.searchState);
          });
    } 
  }

  reset(): void {

    this.items = [];
    this.languages=[];
    this.topics=[];
  }

  toggleAdvanced(): void {
    if (!this.advanced) {
      this.advanced = true;
    } else {
      this.advanced = false;
    }
    this.saveState('advanced',this.advanced);
  }

  addLang(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.languages.push(value);
    }

    event.chipInput!.clear();

    this.languageCtrl.setValue(null);
    this.saveState('languages',this.languages);
  }

  removeLang(language: string): void {
    const index = this.languages.indexOf(language);

    if (index >= 0) {
      this.languages.splice(index, 1);
    }
    this.saveState('languages',this.languages);
  }

  selectedLang(event: MatAutocompleteSelectedEvent): void {
    this.languages.push(event.option.viewValue);
    this.languageInput.nativeElement.value = '';

    this.languageCtrl.setValue(null);
    this.saveState('languages',this.languages);

  }

  private _filterLanguage(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allLanguages.filter(language => language.toLowerCase().includes(filterValue));
  }

  addTopic(event: MatChipInputEvent): void {
    const value = (event.value || '').trim(); 
    if (value) {
      this.topics.push(value);
    }
    event.chipInput!.clear();
    this.saveState('topics',this.topics);

  }

  removeTopic(topic: string): void {
    const index = this.topics.indexOf(topic);

    if (index >= 0) {
      this.topics.splice(index, 1);
    }
    this.saveState('topics',this.topics);
  }
}
