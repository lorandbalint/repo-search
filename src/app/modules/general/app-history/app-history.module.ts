import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppHistoryComponent } from './app-history/app-history.component';



@NgModule({
  declarations: [
    AppHistoryComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AppHistoryModule { }
