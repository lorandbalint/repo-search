import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppHistoryComponent } from './app-history/app-history.component';

const routes: Routes = [
  { path: '', component: AppHistoryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppHistoryRoutingModule { }