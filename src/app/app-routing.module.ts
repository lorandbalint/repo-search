import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from './modules/general/not-found/not-found.component';
import { Page1Component } from './modules/general/page1/page1.component';
import { Page2Component } from './modules/general/page2/page2.component';
import { AppHistoryComponent } from './modules/general/app-history/app-history/app-history.component';


const routes: Routes = [
  { path: '',   redirectTo: '/search', pathMatch: 'full' },
  { path: 'history', component: AppHistoryComponent },
  {
    path: 'search',
    loadChildren: () => import('./modules/general/app-search/app-search.module')
      .then(mod => mod.AppSearchModule)
  },
  { path: 'page1', component: Page1Component },
  { path: 'page2', component: Page2Component },
  { path: '**', component: NotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }